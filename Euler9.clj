(use '[clojure.string :only (split triml)])
(
        let [
          t_t (read-line) 
          t (Integer/parseInt t_t) 
        ]
      (
        loop [a0 t]
        (when (> a0 0)
          (
            let [
              n_t (read-line) 
              n (Integer/parseInt n_t) 
            ]            
            (defn is-triplet? [a b c] (= (+ (* a a) (* b b)) (* c c)))

	    (println (first (for [a (range 1 1000) b (range 1 1000) :let [c (- 1000 (+ a b))]
             :when (and (is-triplet? a b c) (= (+ a b c) 1000))]
             (* a b c) )))          
            )
        (recur (- a0 1) ) )
      )
)

