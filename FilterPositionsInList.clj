; Enter your code here. Read input from STDIN. Print output to STDOUT
;
(fn [lst]
    (loop [odd false lst lst]
        (if-not (nil? (first lst))
            (do
                (if odd (println (first lst)))
                (recur (not odd) (rest lst))))))
