(use '[clojure.string :only (split triml)])
(
        let [
          t_t (read-line) 
          t (Integer/parseInt t_t) 
        ]
      (
        loop [a0 t]
        (when (> a0 0)
          (
            let [
              n_t (read-line) 
              n (Integer/parseInt n_t) 
            ]
            (defn expo [x]
            (reduce * (repeat 2 x)))
            
            (let [sos (/ (*(* n (+ 1 n)) (+ (* 2 n) 1)) 6)]
            (println (- (expo (reduce + (range 1 (+ 1 n)))) sos))
               )            
          )
        (recur (- a0 1) ) )
      )

)

