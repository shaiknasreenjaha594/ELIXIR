(use '[clojure.string :only (split triml)])

(
        let [
          t_t (read-line) 
          t (Integer/parseInt t_t) 
        ]

      (
        loop [a0 t]
        (when (> a0 0)

          (
            let [
              n_t (read-line) 
              n (Integer/parseInt n_t) 
            ]
            
            
             (defn get-factors [number]
                (filter #(= 0 (mod number %)) (range 1 (+ 1 number))))

            (defn is-prime [n]
                (empty? (filter #(= 0 (mod n  %)) (range 2 n))))

            (defn largest-prime-factor [number]
                (last (filter is-prime (get-factors number))))

            
            (println (largest-prime-factor n))    
            
            
            
           
            
            
          )

        (recur (- a0 1) ) )
      )

)

