(use '[clojure.string :only (split triml)])

(
        let [
          t_t (read-line) 
          t (Integer/parseInt t_t) 
        ]

      (
        loop [a0 t]
        (when (> a0 0)

          (
            let [
              n_t (read-line) 
              n (Integer/parseInt n_t) 
            ]
           (def fib-seq
            (lazy-cat [0 1] (map + (rest fib-seq) fib-seq)))
            (println (reduce +(filter even? (take-while #(< % n) fib-seq))))
          )

        (recur (- a0 1) ) )
      )

)

