(use '[clojure.string :only (split triml)])

(
        let [
          t_t (read-line) 
          t (Integer/parseInt t_t) 
        ]

      (
        loop [a0 t]
        (when (> a0 0)

          (
            let [
              n_t (read-line) 
              n (Integer/parseInt n_t) 
            ]
            (defn palindrome? [s] (= (reverse (str s) ) (seq (str s))))

        (println (apply max (filter palindrome?
               (for
                   [a (range 100 n)
                    b (range 100 n)]
                 (* a b)))))
                 )

        (recur (- a0 1) ) )
      )

)

